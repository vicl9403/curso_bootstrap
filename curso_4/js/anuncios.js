var app = new Vue({
	el: '#app',
	data: {
		location : 'Todo México',
		locationList : [
			'Guadalajara',
			'Obregón',
			'Ciudad de México',
		],
		category: 'Todas las categorias',
		categoryList: [ 
			{
				name : 'Carros',
				img : 'https://cdn2.iconfinder.com/data/icons/circle-icons-1/64/car-128.png'
			},
			{
				name : 'Inmuebles',
				img : 'http://www.iconarchive.com/download/i7757/hopstarter/sleek-xp-basic/Home.ico'
			},
		],
		states : [
			'Aguascalientes',
			'Baja California',
			'Baja California Sur',
			'Campeche',
			'Chiapas',
			'Chihuahua',
			'Ciudad de México',
			'Coahuila',
			'Colima',
			'Durango',
			'Estado de México',
			'Guanajuato',
			'Guerrero',
			'Hidalgo',
			'Jalisco',
			'Michoacán',
			'Morelos',
			'Nayarit',
			'Nuevo León',
			'Oaxaca',
			'Puebla',
			'Querétaro',
			'Quintana Roo',
			'San Luis Potosí',
			'Sinaloa',
			'Sonora',
			'Tabasco',
			'Tamaulipas',
			'Tlaxcala',
			'Veracruz',
			'Yucatán',
			'Zacatecas',
		],
		selectedState : 'Selecciona un estado',
		selectedCategory : 'Selecciona una categoria',
	},
	methods: {
		showModal( modal ) {
			$( '#' + modal ).modal('show');
		}
	},
	mounted() {
		$("#ex2").slider({});

		$("#ex2").on("slide", function(slideEvt) {
			$("#slideValueMin").text(slideEvt.value[0]);
			$("#slideValueMax").text("Más de " + slideEvt.value[1]);
		});

		// $('#modal-location').modal('show')
	}
})