<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.7.2/css/bootstrap-slider.css">
	<link rel="stylesheet" href="css/style.css">
</head>

<body>	

	<?php include './sections/menu.php' ?>
	
	<div class="container" id="app">
		
		<!-- Formulario -->
		<div class="row">
			<div class="col-md-12 bg-gray b-radius-5 padding-15">
				
				<div class="row">
					<form>
						<div class="col-md-10">

							<div class="row">
								
								<div class="form-group">
									<div class="col-md-4">
										<input type="text" class="form-control no-radius" placeholder="¿Qúe estas buscando?">
									</div>
									<div class="col-md-4">
										
										<!-- Single button -->
										<div class="btn-group w-100 text-left">
											<button type="button" class="btn btn-default dropdown-toggle w-100 no-radius text-left" 
													data-toggle="dropdown" 
													aria-haspopup="true" 
													aria-expanded="false"
											> {{ category }} <span class="caret pull-right margin-top-10"></span></button>

											<ul class="dropdown-menu w-100 no-radius">
												
												<li v-for="cat in categoryList">
													<a 	href="#" 
														class="dropdown-list"
														@click=" category = cat.name "
													>
													<img v-bind:src="cat.img" class="icon-list" alt="">
														{{ cat.name }}
													</a>
												</li>
												
											</ul>
										</div>
									</div>
									<div class="col-md-4">
										
										<!-- Single button -->
										<div class="btn-group w-100 text-left">
											<button type="button" class="btn btn-default dropdown-toggle w-100 no-radius text-left" 
													data-toggle="dropdown" 
													aria-haspopup="true" 
													aria-expanded="false"
											> {{ location }} <span class="caret pull-right margin-top-10"></span></button>

											<ul class="dropdown-menu w-100 no-radius">
												
												<li v-for="loc in locationList">
													<a 	href="#" 
														class="dropdown-list"
														@click=" location = loc "
													>
														{{ loc }}
													</a>
												</li>
												
											</ul>
										</div>
										
									</div>
								</div>
								
							</div>
					
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-search no-radius w-100">
								<span class="glyphicon glyphicon-search"></span> Buscar
							</button>
						</div>

					</form>
				</div>

			</div>
		</div>
		<!-- Termina formulario -->
		
		<!-- Tabs -->
		<div class="row">
			
			<div class="col-md-12 no-padding margin-top-15">
				
				<button class="box-link-active">
					Todos: 351
				</button>

				<button class="box-link margin-left-10">
					Anuncios profesionales
				</button>

				<button class="box-link margin-left-10">
					Anuncios profesionales
				</button>

				<button class="btn-list pull-right">
					<span class="glyphicon glyphicon-list"></span>
				</button>

				<button class="btn-th pull-right">
					<span class="glyphicon glyphicon-th-large"></span>
				</button>

			</div>

		</div>
		<!-- Terminan las tabs -->

		<!-- Bloque de productos -->
		<div class="row margin-top-30">
			<!-- Bloque de filtros -->
			<div class="col-md-3 no-padding">
				<div class="filter-container">
					<button class="btn btn-filter">
						!Filtrar aquí¡ <span class="glyphicon glyphicon-arrow-down"></span>
					</button>

					<div class="row margin-top-10">
						<div class="form-group">
							<div class="col-md-12 margin-top-10">
								<input  type="text" 
										class="form-control" 
										placeholder="¿Que estas buscando?"
								>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 margin-top-10">
								<div 	class="btn-launch-modal"
										@click="showModal('modal-location')"
								>
									<small>ubicación</small> <br>
									<span>{{ selectedState }}</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 margin-top-10">
								<div 	class="btn-launch-modal"
										@click="showModal('modal-category')"
								>
									<small>categoria</small> <br>
									<span>{{ selectedCategory }}</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 margin-top-10">
								<select name="" id="" class="form-control">
									<option value="">Opción</option>
									<option value="">Opción</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 margin-top-10">
								<select name="" id="" class="form-control">
									<option value="">Opción</option>
									<option value="">Opción</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 padding-10 margin-top-15">
								<span class="pull-left" id="slideValueMin">10</span>
								<span class="pull-right" id="slideValueMax">Más de 20000</span>
								<input id="ex2" 
										type="text" 
										class="span2 w-100" 
										value="" 
										data-slider-min="10" 
										data-slider-max="20000" 
										data-slider-step="5" 
										data-slider-value="[10,20000]"
										data-slider-id="bg-gray"
										data-slider-tooltip="hide"
							 	/>
							</div>
						</div>

					</div>

				</div>
			</div>
			<!-- Bloque de filtros -->
			
			<!-- Bloque de productos -->
			<div class="col-md-9">
				
				<div class="product-card">
					
					<div class="row">
						
						<!-- Imagen del producto -->
						<div class="col-md-2 ">
							<img src="img/product.jpg" class="img-responsive product-image" alt="">
						</div>	
						
						<!-- Contenido del producto -->
						<div class="col-md-10">	
							<h4>Casa amplia</h4>
							<b>$2,700,000</b>
						</div>
					</div>

				</div>

			</div>
			<!-- Termina bloque de productos -->

		</div>
		<!-- Termina bloque de productos -->
		
		<div class="modal fade" tabindex="-1" id="modal-location" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content no-radius">
					<div class="modal-header no-padding">

						<div class="padding-15">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h5 class="modal-title">Todo México</h5>
						</div>
					
						<span class="gray-separator"></span>
					
					</div>
					<div class="modal-body no-padding modal-body-scroll">
							
						<ul class="list-location">
							<li v-for="state in states"
								@click=" selectedState = state "
								data-dismiss="modal"
							> 
								{{ state }}
							</li>
						</ul>

					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
			
		<div class="modal fade" tabindex="-1" id="modal-category" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content no-radius">
					<div class="modal-header no-padding">

						<div class="padding-15">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h5 class="modal-title">Selecciona una categoria</h5>
						</div>
					
						<span class="gray-separator"></span>
					
					</div>
					<div class="modal-body no-padding modal-body-scroll">
							
						<ul class="w-100 no-radius">
							
							<li v-for="cat in categoryList" class="modal-category-list">
								<a 	href="#" 
									@click=" selectedCategory = cat.name  "
									data-dismiss="modal"
								>
								<img v-bind:src="cat.img" class="icon-list" alt="">
									{{ cat.name }}
								</a>
							</li>
							
						</ul>

					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

											

	</div>

	<?php include './sections/footer.php' ?>
	
	<script src="js/anuncios.js"></script>

</body>

</html>

