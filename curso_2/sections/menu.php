<nav class="navbar navbar-default">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">

			<a class="navbar-brand" href="#">
				<img alt="Brand" src="https://www.segundamano.mx/static/img/smmx-logo.png" class="logo">
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

			<ul class="nav navbar-nav">

				<li><a href="#">Mi cuenta</a></li>
				<li><a href="/curso_2/anuncios.php">Anuncios</a></li>
				<li><a href="#">Favoritos</a></li>
				<li>
					<a href="#">
						<span class="glyphicon glyphicon-home"></span>
						Tiendas
					</a>
				</li>
				<li><a href="#">Ayuda</a></li>

			</ul>

			<div class="nav navbar-nav navbar-right">

				<button type="submit" class="btn btn-danger margin-top-10">
					Publica tu anuncio gratis
				</button>

			</div>

		</div><!-- /.navbar-collapse -->
		<hr>
	</div><!-- /.container -->
</nav>