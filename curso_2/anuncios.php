<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/style.css">
</head>

<body>	

	<?php include './sections/menu.php' ?>
	
	<div class="container">

		<!-- Formulario -->
		<div class="row">
			<div class="col-md-12 bg-gray b-radius-5 padding-15">
				
				<div class="row">
					<form>
						<div class="col-md-10">

							<div class="row">
								
								<div class="form-group">
									<div class="col-md-4">
										<input type="text" class="form-control no-radius" placeholder="¿Qúe estas buscando?">
									</div>
									<div class="col-md-4">
										<select class="form-control no-radius">
											<option value="" selected>Todas las categorias</option>
										</select>
									</div>
									<div class="col-md-4">
										
										<!-- Single button -->
										<div class="btn-group w-100 text-left">
											<button type="button" class="btn btn-default dropdown-toggle w-100 no-radius text-left" 
													data-toggle="dropdown" 
													aria-haspopup="true" 
													aria-expanded="false"
											>Todo México <span class="caret pull-right margin-top-10"></span></button>

											<ul class="dropdown-menu w-100 no-radius">
												<?php  for( $i=0; $i<10; $i++) : ?>
													<li><a href="#">
														Valor: <?php echo $i ?>
													</a></li>
												<?php endfor ?>
											</ul>
										</div>
										
									</div>
								</div>
								
							</div>
					
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-search no-radius w-100">
								<span class="glyphicon glyphicon-search"></span> Buscar
							</button>
						</div>

					</form>
				</div>

			</div>
		</div>
		<!-- Termina formulario -->

		<div class="row">
			
			<div class="col-md-12 no-padding margin-top-15">
				
				<button class="box-link-active">
					Todos: 351
				</button>

				<button class="box-link margin-left-10">
					Anuncios profesionales
				</button>

				<button class="box-link margin-left-10">
					Anuncios profesionales
				</button>

				<button class="btn-list pull-right">
					<span class="glyphicon glyphicon-list"></span>
				</button>

				<button class="btn-th pull-right">
					<span class="glyphicon glyphicon-th-large"></span>
				</button>

			</div>

		</div>
		
	</div>

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
</body>

</html>

