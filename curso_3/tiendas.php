<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/style.css">
</head>

<body>	

	<?php include './sections/menu.php' ?>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3><span class="glyphicon glyphicon-home"></span> Tiendas virtuales</h3>
				<p>500 tiendas virtuales en todo México</p>
			</div>
		</div>
		
		<!-- Grid de los productos -->
		<div class="row">
			<div class="col-md-12">
				
				<div class="row">
					
					<?php for ($i=0; $i < 12; $i++) : ?> 
						
						<!-- Producto -->
						<div class="col-md-3 padding-20 ">
							<div class="row">
								<div class="col-xs-12 card">
									<button class="btn btn-favorite">
										<span class="glyphicon glyphicon-heart-empty"></span>
									</button>
									<img src="img/product.jpg" class="img-responsive" alt="">
									<div class="padding-15">
										<h5 class="text-uppercase">Título</h5>
										<a href="">
											<span class="glyphicon glyphicon-phone"></span>
											Cualquier lado
										</a>
										<p>Información</p>
									</div>
								</div>
							</div>
							
						</div>
						<!-- Termina producto -->
						
					<?php endfor ?>
					

				</div>

			</div>
		</div>
		<!-- Grid de productos -->
	</div>

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
</body>

</html>

