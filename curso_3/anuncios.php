<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.7.2/css/bootstrap-slider.css">
	<link rel="stylesheet" href="css/style.css">
</head>

<body>	

	<?php include './sections/menu.php' ?>
	
	<div class="container" id="app">
		
		<!-- Formulario -->
		<div class="row">
			<div class="col-md-12 bg-gray b-radius-5 padding-15">
				
				<div class="row">
					<form>
						<div class="col-md-10">

							<div class="row">
								
								<div class="form-group">
									<div class="col-md-4">
										<input type="text" class="form-control no-radius" placeholder="¿Qúe estas buscando?">
									</div>
									<div class="col-md-4">
										
										<!-- Single button -->
										<div class="btn-group w-100 text-left">
											<button type="button" class="btn btn-default dropdown-toggle w-100 no-radius text-left" 
													data-toggle="dropdown" 
													aria-haspopup="true" 
													aria-expanded="false"
											> {{ category }} <span class="caret pull-right margin-top-10"></span></button>

											<ul class="dropdown-menu w-100 no-radius">
												
												<li v-for="cat in categoryList">
													<a 	href="#" 
														class="dropdown-list"
														@click=" category = cat.name "
													>
													<img v-bind:src="cat.img" class="icon-list" alt="">
														{{ cat.name }}
													</a>
												</li>
												
											</ul>
										</div>
									</div>
									<div class="col-md-4">
										
										<!-- Single button -->
										<div class="btn-group w-100 text-left">
											<button type="button" class="btn btn-default dropdown-toggle w-100 no-radius text-left" 
													data-toggle="dropdown" 
													aria-haspopup="true" 
													aria-expanded="false"
											> {{ location }} <span class="caret pull-right margin-top-10"></span></button>

											<ul class="dropdown-menu w-100 no-radius">
												
												<li v-for="loc in locationList">
													<a 	href="#" 
														class="dropdown-list"
														@click=" location = loc "
													>
														{{ loc }}
													</a>
												</li>
												
											</ul>
										</div>
										
									</div>
								</div>
								
							</div>
					
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-search no-radius w-100">
								<span class="glyphicon glyphicon-search"></span> Buscar
							</button>
						</div>

					</form>
				</div>

			</div>
		</div>
		<!-- Termina formulario -->
		
		<!-- Tabs -->
		<div class="row">
			
			<div class="col-md-12 no-padding margin-top-15">
				
				<button class="box-link-active">
					Todos: 351
				</button>

				<button class="box-link margin-left-10">
					Anuncios profesionales
				</button>

				<button class="box-link margin-left-10">
					Anuncios profesionales
				</button>

				<button class="btn-list pull-right">
					<span class="glyphicon glyphicon-list"></span>
				</button>

				<button class="btn-th pull-right">
					<span class="glyphicon glyphicon-th-large"></span>
				</button>

			</div>

		</div>
		<!-- Terminan las tabs -->

		<!-- Bloque de productos -->
		<div class="row margin-top-30">
			<!-- Bloque de filtros -->
			<div class="col-md-3 no-padding">
				<div class="filter-container">
					<button class="btn btn-filter">
						!Filtrar aquí¡ <span class="glyphicon glyphicon-arrow-down"></span>
					</button>

					<div class="row margin-top-10">
						<div class="form-group">
							<div class="col-md-12 margin-top-10">
								<input  type="text" 
										class="form-control" 
										placeholder="¿Que estas buscando?"
								>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 margin-top-10">
								<input  type="text" 
										class="form-control" 
										placeholder="Ubicación"
								>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 margin-top-10">
								<select name="" id="" class="form-control">
									<option value="">Opción</option>
									<option value="">Opción</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 margin-top-10">
								<select name="" id="" class="form-control">
									<option value="">Opción</option>
									<option value="">Opción</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 padding-10 margin-top-15">
								<span class="pull-left" id="slideValueMin">10</span>
								<span class="pull-right" id="slideValueMax">Más de 20000</span>
								<input id="ex2" 
										type="text" 
										class="span2 w-100" 
										value="" 
										data-slider-min="10" 
										data-slider-max="20000" 
										data-slider-step="5" 
										data-slider-value="[10,20000]"
										data-slider-id="bg-gray"
										data-slider-tooltip="hide"
							 	/>
							</div>
						</div>

					</div>

				</div>
			</div>
			<!-- Bloque de filtros -->

			<div class="col-md-9">
				Mundo
			</div>
		</div>
		<!-- Termina bloque de productos -->
		
	</div>

	<?php include './sections/footer.php' ?>

	<script>
		var app = new Vue({
			el: '#app',
			data: {
				location : 'Todo México',
				locationList : [
					'Guadalajara',
					'Obregón',
					'Ciudad de México',
				],
				category: 'Todas las categorias',
				categoryList: [ 
					{
						name : 'Carros',
						img : 'https://cdn2.iconfinder.com/data/icons/circle-icons-1/64/car-128.png'
					},
					{
						name : 'Inmuebles',
						img : 'http://www.iconarchive.com/download/i7757/hopstarter/sleek-xp-basic/Home.ico'
					},
				]
			},
			mounted() {
				$("#ex2").slider({});

				$("#ex2").on("slide", function(slideEvt) {
					$("#slideValueMin").text(slideEvt.value[0]);
					$("#slideValueMax").text("Más de " + slideEvt.value[1]);
				});

			}
		})
	</script>

</body>

</html>

